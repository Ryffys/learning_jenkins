FROM openjdk:8
WORKDIR /
ADD http://172.17.0.1:8081/artifactory/libs-snapshot-local/message-processor/message-processor/1.0-SNAPSHOT/message-processor-1.0-20190301.164253-1.jar app/message-processor.jar 
COPY /etc/config.properties etc/
CMD java -jar /app/message-processor.jar /etc/config.properties
